/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duplicatetest.theadpool;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import duplicatetest.Configuration;
import duplicatetest.MongoDB;
import genericthreadpool.SyncQueueNode;
import genericthreadpool.WorkerThread;
import java.util.Date;
import org.bson.Document;

/**
 *
 * @author mahbuburrubtalha
 */
public class DocWorker extends WorkerThread {

    private MongoDB mongoDB = MongoDB.getInstance();

    public DocWorker() {
        super();
    }

    @Override
    public void process(SyncQueueNode syncQueueNode) {

        // New Doc for update
        Doc doc = (Doc) syncQueueNode;

        // Get Doc Collection using MongoDB singleton instance
        MongoCollection<Document> collection = mongoDB.getDatabase(Configuration.DB_NAME).getCollection(Configuration.COLLECTION_DOC);

        // set doc count
        Document incUpdate = new Document("doc_freq", doc.getCount());

        Document setUpdate = new Document("doc", doc.getData())
                .append("created_at", new Date());

        // preparing update doc query
        Document update = new Document();
        update.append("$setOnInsert", setUpdate);
        update.append("$inc", incUpdate);

        // setting upsert to true
        UpdateOptions updateOptions = new UpdateOptions();
        updateOptions.upsert(true);

        // doc searching query
        Document query = new Document("doc", doc.getData());

        // update or insert if not exists
        collection.updateOne(query, update, updateOptions);
    }

}
