/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duplicatetest.theadpool;

import genericthreadpool.SyncQueueNode;

/**
 *
 * @author mahbuburrubtalha
 */
public class Doc extends SyncQueueNode {

    private String data;
    private int count;

    public Doc() {
        super();
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
