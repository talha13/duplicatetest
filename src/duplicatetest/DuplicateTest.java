/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duplicatetest;

import duplicatetest.theadpool.Doc;
import duplicatetest.theadpool.DocWorker;
import genericthreadpool.ThreadPool;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mahbuburrubtalha
 */
public class DuplicateTest {

    private ThreadPool threadPool;

    public DuplicateTest() throws InstantiationException, IllegalAccessException {

        threadPool = new ThreadPool(Configuration.DOCUMENT_THREAD_POOL, Configuration.DOCUMENT_THREAD_QUEUE_SIZE, "Doc Thread Pool");

        // setting worker class for db update
        threadPool.setWorkerThreadClass(DocWorker.class);

        threadPool.start();
    }

    public void test() {

        int i;

        for (i = 0; i < 10000; i++) {

            // Creating new doc and setting value
            Doc doc = new Doc();
            doc.setData("talha");
            doc.setCount(1);

            // adding new doc to threadpool
            threadPool.addNode(doc);
        }

        threadPool.stopPool();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here

            DuplicateTest duplicateTest = new DuplicateTest();
            duplicateTest.test();

        } catch (InstantiationException ex) {
            Logger.getLogger(DuplicateTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(DuplicateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
