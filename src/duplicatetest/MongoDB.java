/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duplicatetest;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;

/**
 *
 * @author mahbuburrubtalha
 * 
 * Signleton MongoDB Client
 * 
 */

public class MongoDB extends MongoClient {

    private volatile static MongoDB mongoDB = null;

    private MongoDB(ServerAddress serverAddress, MongoClientOptions clientOptions) {
        super(serverAddress, clientOptions);
    }

    public static MongoDB getInstance() {

        if (mongoDB == null) {

            synchronized (MongoDB.class) {

                MongoClientOptions clientOptions = MongoClientOptions.builder().connectionsPerHost(Configuration.MAX_CONNECTIONS).build();
                ServerAddress serverAddress = new ServerAddress(Configuration.DB_USER, Configuration.DB_PORT);
                mongoDB = new MongoDB(serverAddress, clientOptions);
            }
        }

        return mongoDB;
    }

}
