/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package duplicatetest;

/**
 *
 * @author mahbuburrubtalha
 */
public class Configuration {

    // Thread pool
    public static int DOCUMENT_THREAD_POOL = 150;
    public static int DOCUMENT_THREAD_QUEUE_SIZE = 1000;

    // Mongo DB 
    public static final String DB_HOST = "127.0.0.1";
    public static final String DB_NAME = "db_doc";
    public static final String DB_USER = "";
    public static final String DB_PASSWORD = "";
    public static final int DB_PORT = 27017;
    public static final int MAX_CONNECTIONS = 15000;

    //Collections
    public static final String COLLECTION_DOC = "doc";

}
