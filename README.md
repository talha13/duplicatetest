# README #

This project is created using Netbeans Version 8.0.2

Mongo DB Collection

{
  _id: ObjectId,
  doc: String,
  doc_freq: Integer,
  created_at: Date
}

Index:

db.doc.ensureIndex({doc:1});